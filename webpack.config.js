var path = require('path');

module.exports = {
    devtool: 'source-map',
    entry: './app/index.js',
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'index.js',
        publicPath: '/build/'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'stage-0']
                }
            }
        ]
    },
    resolve: {
        modulesDirectories: ['node_modules'],
        alias: {
            actions: path.join(__dirname, '/app/actions'),
            components: path.join(__dirname, '/app/components'),
            constants: path.join(__dirname, '/app/constants'),
            middleware: path.join(__dirname, '/app/middleware'),
            reducers: path.join(__dirname, '/app/reducers'),
            store: path.join(__dirname, '/app/store')
        },
        extensions: ['', '.js']
    },
    watch: true
};
