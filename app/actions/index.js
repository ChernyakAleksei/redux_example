import {
    LOAD_ITEMS,
    ADD_ITEM,
    EDIT_ITEM,
    REMOVE_ITEM,
} from 'constants';
// эти функции называются actionCreators. они должны возвращать action. т.е. объект вроде {type: 'SOME_ACTION'...}

// этот actionCreator возвращает не объект, а функцию, которуй redux вызовет и передаст ей параметром store.dispatch
// такое возможно из-за того, что мы используем redux-thunk в middleware
export function loadItems() {
    return function(dispatch) {
        fetch('data/items.json')
            .then(res => res.json())
            .then(items => {
                dispatch({
                    type: LOAD_ITEMS,
                    payload: items
                });
            })
            .catch(err => { console.log(err); })
    }
}

export function addItem(itemData) {
    return {
        type: ADD_ITEM,
        payload: itemData
    }
}

export function editItem(itemId, newText) {
    return {
        type: EDIT_ITEM,
        payload: {id: itemId, text: newText}
    }
}

export function removeItem(itemId) {
    return {
        type: REMOVE_ITEM,
        payload: {id: itemId}
    }
}