import {store} from 'store';
import {
    addItem
} from 'actions';

export default class Form{
    constructor(formElement){
        this.formElement = document.getElementById(formElement);

        this.render();
        this._initEvents();
    }

    render(){
        this.formElement.innerHTML = `
            <input type="text" class="input" placeholder="введите название пункта"/>
            <button class="button">Добавить пункт в список</button>`;
    }

    _initEvents(){
        let addButton = this.formElement.querySelector('.button');
        let input = this.formElement.querySelector('.input');

        addButton.addEventListener('click', function(e) {
            e.preventDefault();

            let inputValue = input.value;
            let newItem = {id: Math.random(), text: inputValue};

            store.dispatch(addItem(newItem));

            input.value = '';
        });
    }
}