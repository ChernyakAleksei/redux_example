import {store} from 'store';
import {
    loadItems,
    editItem,
    removeItem
} from 'actions';

export default class ListComponent{
    constructor(listElementId){
        this.listElement = document.getElementById(listElementId);
        this.initEvents();

        store.dispatch(loadItems());
    }

    initEvents(){
        this.listElement.addEventListener('click', function(e) {
            e.preventDefault();

            let target = e.target;
            let targetCssClass = target.className;
            let li = target.parentNode;
            let itemId = li.itemId;

            if(targetCssClass === 'remove'){
                store.dispatch(removeItem(itemId));
                return;
            }

            if(targetCssClass === 'edit'){
                let input = li.childNodes[0];
                let text = input.value;

                store.dispatch(editItem(itemId, text));
                return;
            }
        });
    }

    render(data){
        this.listElement.innerHTML = '';

        data.forEach(itemData => {
            let li = document.createElement('li');
            let itemText = itemData.text;

            li.innerHTML = `<input type="text" value="${itemText}">
                            <a href="#" class="edit">редактировать</a>
                            <a href="#" class="remove">удалить</a>`;

            li.itemId = itemData.id;
            this.listElement.appendChild(li);
        });
    }
}
