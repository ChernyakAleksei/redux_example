export default class Counter{
    constructor(counterElementId){
        this.counterElement = document.getElementById(counterElementId);
    }

    render(counterValue){
        this.counterElement.innerHTML = `Отправлено сообщений: <div id="counter" class="counter">${counterValue}</div>`;
    }
}