import {
    LOAD_ITEMS,
    ADD_ITEM,
    REMOVE_ITEM,
} from 'constants';

export function counter(counterState = 0, action) {
    switch(action.type){
        case LOAD_ITEMS:
            return action.payload.length;

        case ADD_ITEM:
            return counterState + 1;

        case REMOVE_ITEM:
            return counterState - 1;

        default:
            return counterState;
    }
}
