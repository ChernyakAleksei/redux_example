import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import logger from 'middleware/logger';
// возможен такой вариант добавления редюсеров стору
// import * as reducers from 'reducers';
// export const store = createStore(combineReducers(reducers)...

// а можно так
import {listItems, counter} from 'reducers';
export const store = createStore(combineReducers({
    listItems: listItems,
    counter
}), compose(
    applyMiddleware(thunk, logger),
    // так подключаем redux dev tool (это там можно путешествовать во времени)
    // сначала надо добавить это расширение в chrome https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?utm_source=chrome-app-launcher-info-dialog
    // затем открываем хромовский отладчик на нашей странице и жмём сюда http://prntscr.com/cuefed
    window.devToolsExtension ? window.devToolsExtension() : f => f
));
