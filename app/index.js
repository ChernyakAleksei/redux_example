// я настроил alias'ы в webpack.config для
// components, store, constants, middleware, reducers.
// Поэтому я могу их так экспортировать import List from 'components/list';
// Если бы я это не сделал - то внутри какого-нибудь компонента я бы получал store примерно так -
// import {store} from '../../../store'; - не удобно!
import List from 'components/list';
import Counter from 'components/counter';
import Form from 'components/form';

import {store} from 'store';

let form = new Form('form');
let counter = new Counter('counter');
let list = new List('list');
let chatList = new List('chat-list');

store.subscribe(function() {
    let state = store.getState();

    counter.render(state.counter);
    list.render(state.listItems);
    chatList.render(state.listItems);
});
