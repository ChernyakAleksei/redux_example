# redux_example

Пример работы redux с обычным javascript 
## Установка
Клонируем репозиторий

```
git clone https://github.com/jsruvi/redux_example.git
```

Чтобы собрать проект нужен webpack. Устанавливаем, если ещё не установлен.

```
npm i webpack -g
```

Устанавливаем node-static.

```
npm i node-static -g
```

## Старт

Собираем проект. В терминале перейдите в папку с проектом и вызовите команду:
```
webpack
```

Если планируете делать какие-то изменения в файлах проекта - тогда вызывайте с флагом -w:
```
webpack -w
```

Запустите сервер:
```
static
```

Перейдите по этой [ссылке](http://127.0.0.1:8080/) 


## Полезные ссылки
- [Моя презентация](https://docs.google.com/presentation/d/16bWdBH2Y84Y7unLXKf_IeEAhXETFzQVc5jYD280-svM/edit?usp=sharing)
- [Тут рассказывают про combineReducers](https://youtu.be/-CJzPfOm5rs?t=1222)
- [Подход к организации файлов](https://github.com/erikras/ducks-modular-redux)
- [redux-devtools-extension](https://github.com/zalmoxisus/redux-devtools-extension)
- [Частично-русская документация](https://rajdee.gitbooks.io/redux-in-russian/content/docs/basics/index.html)
- [Сохранять и загружать состояние из localStorage](https://egghead.io/lessons/javascript-redux-persisting-the-state-to-the-local-storage?course=building-react-applications-with-idiomatic-redux)
- [redux-saga](https://github.com/yelouafi/redux-saga)
- [immutable.js](https://facebook.github.io/immutable-js/docs)
- [примеры redux](https://github.com/reactjs/redux/tree/master/examples)
